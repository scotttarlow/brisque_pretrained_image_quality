####BRISQUE No Reference Image Quality

The goal of this repo is to have a library of functions that allows the user to use a pretrained BRISQUE
Model.  Please see the .ipynb cookbook for an example of how to use it, and please see lib for all the functions. This readme will be updated with better documentation eventually! 

Too see theoretical functionality, refer to the original paper: http://dl.icdst.org/pdfs/files/b58b8ccd6a0fea308dc365bc5882ba67.pdf

"No-Reference Image Quality Assessment in the Spatial Domain"


Please go through this library through the two cookbooks, BRISQUE_cookbook.ipynb and BRISQUE_tid2013_notebook.ipynb

Most people will want to use the pretrained models from BRISQUE_tid2013_notebook, which are located in
/pretrained_scikit_models. This is because:

    1) the models are more flexible compared to the original BRISQUE model
    2) they are not dependent on svm and svmutil which seems to be difficult to install
    3) the notebook teaches you how to train your own model on your own data


pretrained models have are named for the type distortion classification or is a regressor.  


Requirements for BRISQUE_tid2013_notebook:

    1. sklearn
    2. pandas
    3. numpy
    4. PIL
     ----  additional for feature enginering ----
    5. urlib
    6. scipy
    7. matplotlib
    8. opencv
    9. skimage

BRISQUE_cookbook.ipynb is the the model from the original paper, and requires addtional requirements of:

    1. svmutil
    2. svm



This library is suited for people who want to have an objective measure of image quality without a reference image - where higher values of the score mean a higher objective image quality, and lower means a lower objective image quality, between 0 and 100. 



